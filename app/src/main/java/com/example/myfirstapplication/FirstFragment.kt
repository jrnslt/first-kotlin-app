package com.example.myfirstapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import kotlin.random.Random

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {
    val correctNumber = genereRateNumber();


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.toast_button).setOnClickListener {
            val myToast = Toast.makeText(context, "Hello Toast!", Toast.LENGTH_SHORT)
            countMin(view);
        }

        view.findViewById<Button>(R.id.random_button).setOnClickListener {
            countPlus(view)
        }

        view.findViewById<Button>(R.id.count_button).setOnClickListener {
            val random = Toast.makeText(
                context, "Chuck Norris can divide by zero.",
                Toast.LENGTH_SHORT
            )
            random.show();
        }


        view.findViewById<Button>(R.id.new_random_button).setOnClickListener{
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
            //findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }
    }

    fun genereRateNumber(): Int {
        val randomInteger = Random.nextInt()
        return randomInteger;
    }

    fun countPlus(view: View) {
        // Get the text view
        val showCountTextView = view.findViewById<TextView>(R.id.textview_first)

        // Get the value of the text view.
        val countString = showCountTextView.text.toString()

        // Convert value to a number and increment it
        var count: Int = Integer.parseInt(countString)
        count++

        // Display the new value in the text view.
         showCountTextView.text = count.toString()
    }

    fun countMin(view: View) {
        val showCountTextView = view.findViewById<TextView>(R.id.textview_first)

        val countString = showCountTextView.text.toString()

        var count: Int = Integer.parseInt(countString)
        count--

        showCountTextView.text = count.toString()
    }



}
